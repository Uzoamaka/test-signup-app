import React from 'react';
import { Route, Switch } from 'react-router-dom';
import SignUp from './components/SignUp.container';
import {SignUpLink} from './components/Signup.link';
import { ThemeProvider, CSSReset } from "@chakra-ui/core";
import { customTheme } from "./components/Theme";

function App() {
  return (
      <ThemeProvider theme={customTheme}>
        <CSSReset />
        <Switch>
        <Route exact path="/" component={SignUp} />
        <Route exact path="/signup/link" component={SignUpLink} />
      </Switch>
      </ThemeProvider>
  );
}

export default App;
