import React, { useState } from "react";
import {
  Box,
  Flex,
  Image,
  Text,
  FormControl,
  Input,
  Button,
  Stack,
  // useToast,
} from "@chakra-ui/core";
import Logo from "../assets/Logo.svg";
import KudaMan from "../assets/bg.svg";
import Apple from "../assets/apple.png";
import Google from "../assets/google.png";
import Kuda from "../assets/kuda.png";
import KudaWeb from "../assets/kuda-web.png";

import "boxicons";
// import { ToastBox } from "./ToastBox";

export function SignUpUi({ isLoading, error, success, addNewUser, history }) {
  // const toast = useToast();
  const [userInfo, setUserInfo] = useState({
    email: "",
    phoneNumber: "",
  });

  function handleChange(event) {
    setUserInfo({ ...userInfo, [event.target.name]: event.target.value });
  }

  function handleSubmit(values) {
    addNewUser(values);
  }

  //   if (!!success && !isLoading) {
  //     toast({
  //       position: "bottom-left",
  //       render: () => <ToastBox message={"You have successfully signed up"} />,
  //     });
  // history.push('/signup/link')
  //   }

  //   if (error && !isLoading) {
  //     setNeutralState(true);
  //     toast({
  //       position: "bottom-left",
  //       render: () => <ToastBox message={error} />,
  //     });
  //   }

  return (
    <Box>
      <Flex
        paddingY="20px"
        width="100%"
        justifyContent="center"
        alignItems="center"
      >
        <Image src={Logo} alt="kuda bank logo" />
      </Flex>
      <Box
        display={{ md: "flex" }}
        flexDirection={['row', 'column', 'row', 'row']}
        alignItems="center"
        justifyContent="center"
      >
        <Box
          width={[
            "30%", // base
            "100%", // 480px upwards
            "40%", // 768px upwards
            "40%", // 992px upwards
          ]}
          flexShrink="0"
        >
          <Image
            width={{ base: "100%", md: "50", sm: "100", lg: "100" }}
            src={KudaMan}
            alt="Man holding a gift box"
            // height="100%"
          />
        </Box>
        <Box
          width={[
            "40%", // base
            "100%", // 480px upwards
            "40%", // 768px upwards
            "40%", // 992px upwards
          ]}
          mt={{ base: 4, md: 0 }}
          ml={{ md: 6 }}
        >
          <Box background="#48D38A" padding="30px" boxShadow="-30px 60px 180px -80px">
            <Box color="#fff">
              <Text
                fontSize="27px"
                fontWeight="semibold"
                lineHeight="40px"
                marginBottom="10px"
              >
                A friend has invited you to join Kuda!
              </Text>
              <Text fontSize="14px">
                Share your email address and phone number to enjoy the bank with
                free transfers, zero card maintenance fees and friendly customer
                service.
              </Text>
            </Box>
            <form>
              <Stack spacing={3} marginTop="14px">
                <FormControl>
                  <Input
                    name="email"
                    placeholder="What's your email address?"
                    size="md"
                    onChange={(event) => handleChange(event)}
                  />
                </FormControl>
                <FormControl>
                  <Input
                    name="phoneNumber"
                    placeholder="What's your phone number?"
                    size="md"
                    onChange={(event) => handleChange(event)}
                  />
                </FormControl>
              </Stack>
              <Button
                bg="#40196D"
                color="#fff"
                marginTop="20px"
                marginBottom="10px"
                isLoading={isLoading}
                onClick={() => handleSubmit(userInfo)}
              >
                Get Started
              </Button>
            </form>
          </Box>
        </Box>
      </Box>
      <Flex marginTop="100px" marginLeft="180px" marginRight="125px" flexDirection="column">
        <Stack isInline spacing={8} align="center" width="40%">
          <Box>
          <box-icon  color="green" type="logo" name="twitter"></box-icon>
          </Box>
          <Box>
          <box-icon color="green" name="facebook" type="logo"></box-icon>
          </Box>
          <Box>
          <box-icon color="green" name="medium" type="logo"></box-icon>
          </Box>
          <Box>
          <box-icon color="green" name="instagram" type="logo"></box-icon>
          </Box>
        </Stack>
        <Box marginTop="30px">
          <Text color="#090909" fontSize="14px" fontWeight="semibold">
            Coming soon
          </Text>
          <Flex marginTop="10px" marginBottom="30px">
          <span
            style={{ width: "110px",  paddingRight: "6px " }}
          >
            <Image width="100%" maxWidth="100%" src={Apple} />
          </span>
          <span
            style={{ width: "110px", paddingLeft: "6px", paddingRight: "6px " }}
          >
            <Image width="100%" maxWidth="100%" src={Google} />
          </span>
          <span
            style={{ width: "110px", paddingLeft: "6px", paddingRight: "6px " }}
          >
            <Image width="100%" maxWidth="100%" src={Kuda} />
          </span>
          <span
            style={{ width: "110px", paddingLeft: "6px", paddingRight: "6px " }}
          >
            <Image width="100%" maxWidth="100%" src={KudaWeb} />
          </span>
          </Flex>
        </Box>
        <Box paddingBottom="20px">
          <Text opacity="0.3" fontSize="12px" color="#1d1d1d">
            © 2020 Kuda Microfinance Bank (RC796975). All rights reserved. All
            deposits are insured by the Nigerian Deposit Insurance Corporation
            (NDIC). Kuda Microfinance Bank is licensed by the Central Bank of
            Nigeria. “Kuda” and “Kudabank” are trademarks of Kuda Technologies
            Ltd Lagos:
          </Text>
          <Text opacity="0.3" fontSize="12px" color="#1d1d1d" marginTop="20px">
            Kuda Current Account is provided through Kuda Microfinance Bank.Kuda
            card is issued by access bank, pursuant to a license from Visa
            International & Verve. All text, graphics, audio files, code,
            downloadable material, and other works on this website are the
            copyrighted works of Kuda Microfinance Bank. All Rights Reserved.
            Any unauthorized redistribution or reproduction of any copyrighted
            materials on this website is strictly prohibited.
          </Text>
        </Box>
      </Flex>
    </Box>
  );
}
