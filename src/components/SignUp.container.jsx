import React, { useEffect } from "react";
import { connect } from "react-redux";
import { addNewUser, handleClientLoad } from '../state/signup.action';
import { SignUpUi } from "./Signup.ui";

function SignUp(props) {
  const { isLoading, addNewUser, success, error, history } = props;

  useEffect(() => {
    handleClientLoad();
  }, []);
  
  return (
    <SignUpUi
      isLoading={isLoading}
      error={error}
      addNewUser={addNewUser}
      history={history}
      success={success}
      {...props}
    />
  );
}

const mapStateToProps = (store) => {
  return {
    isLoading: store.auth.isLoading,
    success: store.auth.success,
    error: store.auth.error,
    user: store.auth.user,
  };
};

export default connect(mapStateToProps, { addNewUser })(SignUp);
