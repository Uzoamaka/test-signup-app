import React from "react";
import { Box, Flex, Image, Text, Stack, Button } from "@chakra-ui/core";
import Logo from "../assets/Logo.svg";
import KudaMan from "../assets/bg.svg";
import "boxicons";
import Apple from "../assets/apple.png";
import Google from "../assets/google.png";
import Kuda from "../assets/kuda.png";
import KudaWeb from "../assets/kuda-web.png";

export function SignUpLink() {
  return (
    <Box>
      <Flex
        paddingY="20px"
        width="100%"
        justifyContent="center"
        alignItems="center"
      >
        <Image src={Logo} alt="kuda bank logo" />
      </Flex>
      <Box
        display={{ md: "flex" }}
        flexDirection={["row", "column", "row", "row"]}
        alignItems="center"
        justifyContent="center"
      >
        <Box
          width={[
            "30%", // base
            "100%", // 480px upwards
            "40%", // 768px upwards
            "40%", // 992px upwards
          ]}
          flexShrink="0"
        >
          <Image
            width={{ base: "100%", md: "50", sm: "100", lg: "100" }}
            src={KudaMan}
            alt="Man holding a gift box"
            // height="100%"
          />
        </Box>
        <Box
          width={[
            "30%", // base
            "100%", // 480px upwards
            "30%", // 768px upwards
            "30%", // 992px upwards
          ]}
          mt={{ base: 4, md: 0 }}
          ml={{ md: 6 }}
        >
          <Box
            background="#48D38A"
            padding="50px"
            boxShadow="-30px 60px 180px -80px"
          >
            <Box>
              <Text
                fontSize="24px"
                fontWeight="medium"
                lineHeight="30px"
                marginBottom="19px"
                textAlign="center"
                color="#fff"
              >
                Click the link to download Kuda
              </Text>
              <Button
                marginBottom="40px"
                bg="#fff"
                borderRadius="5px"
                padding="14px 16px"
              >
                https://kudabank.com/TundeXGrZRk
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
      <Flex
        marginTop="100px"
        marginLeft="180px"
        marginRight="125px"
        flexDirection="column"
      >
        <Stack isInline spacing={8} align="center" width="40%">
          <Box>
            <box-icon color="green" type="logo" name="twitter"></box-icon>
          </Box>
          <Box>
            <box-icon color="green" name="facebook" type="logo"></box-icon>
          </Box>
          <Box>
            <box-icon color="green" name="medium" type="logo"></box-icon>
          </Box>
          <Box>
            <box-icon color="green" name="instagram" type="logo"></box-icon>
          </Box>
        </Stack>
        <Box marginTop="30px">
          <Text color="#090909" fontSize="14px" fontWeight="semibold">
            Coming soon
          </Text>
          <Flex marginTop="10px" marginBottom="30px">
            <span style={{ width: "110px", paddingRight: "6px " }}>
              <Image width="100%" maxWidth="100%" src={Apple} />
            </span>
            <span
              style={{
                width: "110px",
                paddingLeft: "6px",
                paddingRight: "6px ",
              }}
            >
              <Image width="100%" maxWidth="100%" src={Google} />
            </span>
            <span
              style={{
                width: "110px",
                paddingLeft: "6px",
                paddingRight: "6px ",
              }}
            >
              <Image width="100%" maxWidth="100%" src={Kuda} />
            </span>
            <span
              style={{
                width: "110px",
                paddingLeft: "6px",
                paddingRight: "6px ",
              }}
            >
              <Image width="100%" maxWidth="100%" src={KudaWeb} />
            </span>
          </Flex>
        </Box>
        <Box paddingBottom="20px">
          <Text opacity="0.3" fontSize="12px" color="#1d1d1d">
            © 2020 Kuda Microfinance Bank (RC796975). All rights reserved. All
            deposits are insured by the Nigerian Deposit Insurance Corporation
            (NDIC). Kuda Microfinance Bank is licensed by the Central Bank of
            Nigeria. “Kuda” and “Kudabank” are trademarks of Kuda Technologies
            Ltd Lagos:
          </Text>
          <Text opacity="0.3" fontSize="12px" color="#1d1d1d" marginTop="20px">
            Kuda Current Account is provided through Kuda Microfinance Bank.Kuda
            card is issued by access bank, pursuant to a license from Visa
            International & Verve. All text, graphics, audio files, code,
            downloadable material, and other works on this website are the
            copyrighted works of Kuda Microfinance Bank. All Rights Reserved.
            Any unauthorized redistribution or reproduction of any copyrighted
            materials on this website is strictly prohibited.
          </Text>
        </Box>
      </Flex>
    </Box>
  );
}
