export const SPREADSHEET_ID = "1OMqj78CFMaGRi7xzIRatl9dRxYZOKVJjTy-Ba3fXCGQ";
export const CLIENT_ID =
  "951038213205-slbmmlte4i25k785phnc3sqje853ncpe.apps.googleusercontent.com";
export const API_KEY = "AIzaSyAN0CMFQFryCosh7M4xrVggNPYTnV7nF_I";
export const SCOPE = "https://www.googleapis.com/auth/spreadsheets";
export const range = "A2";

export const headers = {
  "Content-Type": "application/json",
  Authorization: `Bearer ${CLIENT_ID}`,
  "Access-Control-Allow-Origin": `${CLIENT_ID}`
};

export const params = {
  // auth: auth,
  spreadsheetId: `${SPREADSHEET_ID}`,
  range: "A2",
  valueInputOption: 'RAW',
  insertDataOption: 'INSERT_ROWS',
  includesValuesInResponse: true,
  responseValueRenderOption: 'FORMATTED_VALUE'
}