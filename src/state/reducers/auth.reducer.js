import * as types from "../action.types";

const initialState = {
  user: null,
  isLoading: false,
  error: false,
  success: false,
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_USER_STARTED:
      return {
        ...state,
        isLoading: true,
      };
    case types.ADD_USER_SUCEEDED:
      return {
        ...state,
        isLoading: false,
        success: true,
        user: action.payload,
      };
    case types.ADD_USER_FAILED:
      return {
        ...state,
        isLoading: false,
        success: false,
        error: true,
      };
    default:
      return state;
  }
};
