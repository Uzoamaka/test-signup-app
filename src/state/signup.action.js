/* global gapi */

import * as types from "./action.types";
// import Axios from "axios";
import { params, API_KEY, CLIENT_ID, SCOPE } from "../config";

function  initWindowsClient() {
  gapi.client.init({
    'apiKey': API_KEY,
    'clientId': CLIENT_ID,
    'scope': SCOPE,
    'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
  }).then(() => {
    gapi.auth2.getAuthInstance().isSignedIn.listen()
    // this.updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
  })
}

export function handleClientLoad() {
  gapi.load('client:auth2', initWindowsClient);
}

export const addNewUser = ({ email, phoneNumber }) => async (dispatch) => {
  dispatch({ type: types.ADD_USER_STARTED });
  const valueRangeBody = {
    'majorDimension': 'ROWS',
    'values': [email, phoneNumber],
    'key': API_KEY,
    'clientId': CLIENT_ID,
    'scope': SCOPE,
  };
 
  let request = gapi.client.sheets.spreadsheets.values.append(params, valueRangeBody);
  request.then((res) => {
    dispatch({ type: types.ADD_USER_SUCEEDED, payload: res.result });
  }, (reason) => {
    console.error('error:' + reason.result.error.message)
    dispatch({ type: types.ADD_USER_FAILED, payload: reason.result.error.message });
  })
};





 // try {
  //   const response = await Axios.post(
  //     `https://cors-anywhere.herokuapp.com/sheets.googleapis.com/v4/spreadsheets/${SPREADSHEET_ID}/values/${range}?key=${API_KEY}&valueInputOption=RAW&insertDataOption=INSERT_ROWS&includeValuesInResponse=true:append`,
  //     JSON.stringify({
  //       "requests": [
  //         {
  //           "updateCells": {
  //             "range": "A1",
  //             " ": [
  //               {
  //                 "values": [
  //                   [
  //                     `${email}`,
  //                     `${phoneNumber}`
  //                   ]
  //                 ]
  //               }
  //             ]
  //           }
  //         }
  //       ]
  //     }), //     { headers: headers }
  //   );
  //   console.log(response);

  //   dispatch({ type: types.ADD_USER_SUCEEDED, payload: response.data });
  // } catch (err) {
  //   console.log(err);
  //   dispatch({ type: types.ADD_USER_FAILED, payload: err });
  // }